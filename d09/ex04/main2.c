/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/24 14:08:13 by loic              #+#    #+#             */
/*   Updated: 2019/02/13 14:23:50 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_rot42(char *str);

int	main()
{
	char	str[500];

	printf("Enter a sentence to encryt it whith Rot42: \n");
		scanf("%[^\n]s", str);
		//read input including spaces till a new line
	printf("%s\n", ft_rot42(str));
	return (0);
}
