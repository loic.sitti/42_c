/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/16 20:24:14 by loic              #+#    #+#             */
/*   Updated: 2019/01/16 20:25:05 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_generic(void);

int	main()
{
	ft_generic();
	return (0);
}
