/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_button3.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/13 14:57:12 by loic              #+#    #+#             */
/*   Updated: 2019/02/13 15:03:52 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_sorted(int left, int mid, int right)
{
	if (left <= mid && mid <= right)
			return (1);
	else
			return (0);
}

int		ft_button(int i, int j, int k)
{
	if (is_sorted(i, j, k))
			return (j);
	if (is_sorted(k, j, i))
			return (j);
	if (is_sorted(j, i, k))
			return (i);
	if (is_sorted(k, i, j))
			return (i);
	if (is_sorted(i, k, j))
			return (k);
	if (is_sorted(j, k, i))
			return (k);
	return (0);
}
