/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/13 14:45:42 by loic              #+#    #+#             */
/*   Updated: 2019/02/13 14:50:42 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_button(int i, int j, int k);

int	main ()
{

	printf("%d\n", ft_button(1, 2, 3));
	printf("%d\n", ft_button(2, 1, 3));
	printf("%d\n", ft_button(3, 1, 2));
	printf("%d\n", ft_button(3, 2, 1));
	printf("%d\n", ft_button(3, 3, 3));
	printf("%d\n", ft_button(3, 2, 3));
	printf("%d\n", ft_button(-1, -2, -3));
	printf("%d\n", ft_button(10, 2, 30));
	return (0);
}
