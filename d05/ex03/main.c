/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 22:39:31 by loic              #+#    #+#             */
/*   Updated: 2018/11/11 23:46:16 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
//#include <string.h>

char			*ft_strcpy(char *dest, char *src);

int	main()
{
	char src[] = "Thor";
	char dest[] = "Loki";
	printf("%s = dest\n", dest);
	printf("%s  = src\n", src);
	printf("%s = dest now ||copy src to dest\n", ft_strcpy(dest, src));
	return (0);
}
