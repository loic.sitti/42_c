/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/11 22:14:19 by loic              #+#    #+#             */
/*   Updated: 2019/02/15 12:39:51 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 * declare an index
 * set it to 0
 * while src[] at index is not equal to '\0'
 *	set dest[] at index to src[] at index
 *	Thusly, copy over src to dest
 *	index++
 * when rhe while condition is broke,
 * set the current index of dest to the current index of src
 * return the dest[]
 * */

char			*ft_strcpy(char *dest, char *src)
{
	int		i;

	i = 0;

	while (src[i] != '\0')
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}
