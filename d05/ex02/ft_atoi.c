/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 18:46:37 by loic              #+#    #+#             */
/*   Updated: 2019/02/15 00:11:34 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** While the crrent char in the string is a space.
**	increase str till it isn't.
** set the starting point result to zero.
** set the sgne to -1 if we have a '-'
** increase past the sign char.
** increase past the zeros.
** for this next loop, use an i
** while we are not the \0 and wr have a diigit char
**  take the result and set it to:
**   the result * 10 + the current digitchar minus the '0' char.
** if we are dealing with a nuber that is longer than 19 digits
**  or higher the max or min LL,
**  check the sign, pos / neg return -1 or 0 respectivly
**  return the result * the signe.
*/

int				ft_atoi(char *str)
{

	int					i;
	int					sign;
	unsigned long long	res;

	i = 0;

	while (str[0] == '\n' || str[0] == '\t' || str[0] == '\v' || 
			str[0] == ' ' || str[0] == '\f' || str[0] == '\r')
			str++;
	res = 0;
	sign = (str[0] == '-' ? -1 : 1);
	str = (str[0] == '-' || str [0] == '+') ? str + 1 : str;
	while (str[0] == '0')
			str++;
	while (str[i] != '\0' && (str[i] >= '0') && (str[i] <= '9'))
			res = res * 10 + (str[i++] - '0');
	if (i > 19 || res >= 922337203654775808ULL)
						//ULL (allow long long max int)
			return (sign == 1 ? -1 : 0);
	return (res * sign);
}
