/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 22:39:31 by loic              #+#    #+#             */
/*   Updated: 2018/11/15 00:15:10 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char			*ft_strncpy(char *dest, char *src, unsigned int n);

int	main()
{
	char src[] = "Thor-is-a-b***";
	char dest[13];
	unsigned int	n = 6;
//	printf("%s = dest\n", dest);
	printf("%s  = src\n", src);
	printf("%d\n", n);
//	memset(dest, '\0', sizeof(dest));
//	strcpy(src, "this is a test");
//	strncpy(dest, src, 5);
//	printf("Final copied string; %s\n", dest);

	printf("%s= Final copied string\n", ft_strncpy(dest, src, n));
	return (0);
}
