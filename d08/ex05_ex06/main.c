/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/15 05:32:29 by loic              #+#    #+#             */
/*   Updated: 2019/01/16 02:58:16 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_stock_par.h"

int main(int argc, char **argv)
{
	struct	s_stock_par	*stock;

	stock = ft_param_to_tab(argc, argv);
	ft_show_tab(stock);
}
