/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_show_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/15 05:46:09 by loic              #+#    #+#             */
/*   Updated: 2019/01/16 02:57:45 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stock_par.h"

void		ft_putstr(char *str)
{
	while (*str != '\0')
	{
		ft_putchar(*str);
		str++;
	}
}

void		ft_putnbr(int nb)
{
	if ( nb < 0 )
	{
			ft_putchar('-');
			nb *= -1;
	}
	if (nb >= 10)
	{
			ft_putnbr(nb / 10);
			ft_putnbr(nb % 10);
	}
	else
	{
		ft_putchar(nb + 48);
	}
}

void		ft_show_tab(struct s_stock_par *par)
{
	int	i;
	int	j;

	i = 0;
	while (par[i].str != NULL)
	{
			ft_putstr(par[i].copy);
			ft_putstr("\n");
			ft_putnbr(par[i].size_param);
			ft_putstr("\n");
			j = 0;
			while (par[i].tab[j])
			{
					ft_putstr(par[i].tab[j]);
					ft_putstr("\n");
					j++;
			}
			i++;
	}
}
