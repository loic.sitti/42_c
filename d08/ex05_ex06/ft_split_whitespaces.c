/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_whitespace.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/14 23:12:47 by loic              #+#    #+#             */
/*   Updated: 2019/01/14 23:34:36 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define sep(x) (x == '\n' || x == '\t' || x == '\0' || x == ' ')
#include <stdlib.h>

int		cal_len(char *str)
{
	int	i;

	i = 0;
	while (*str)
	{
		if (!sep(*str) && sep(*str + 1))
				i++;
			str++;
	}
	return (i);
}

char	**ft_split_whitespaces(char *str)
{
	int	i;
	int	j;
	int	len;
	char	**tab;

	tab = malloc(cal_len(str));
	*(tab + 1) = (char*)malloc(sizeof(char) * ((cal_len(str) + 1)));
	i = 0;
	while (*str)
	{
		while (sep(*str) && *str)
				str++;
		if (*str == '\0')
				break;

		len = 0;
		while (!sep(str[len]))
				len++;
		tab[i] = malloc(len + 1);
		j = 0;
		while (!sep(*str))
				tab[i][j++] = *str++;
		tab[i][j] = '\0';
		i++;
	}
	tab[i] = 0;
	return (tab);
}
