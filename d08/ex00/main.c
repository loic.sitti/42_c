/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/14 23:09:18 by loic              #+#    #+#             */
/*   Updated: 2019/01/14 23:24:32 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	**ft_split_whitespaces(char *str);

int	main()
{
	char**	res;

	for (res = ft_split_whitespaces("split this"); *res != 0; res++)
	{
		printf("'%s',", *res);
	}
	return (0);
}
