/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_boolean.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/15 01:14:02 by loic              #+#    #+#             */
/*   Updated: 2019/01/15 01:37:53 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_BOOLEAN_H
#define FT_BOOLEAN_H

#include <unistd.h>
#define TRUE 1
#define FALSE 0
#define EVEN(x)	(!(x % 2))
#define EVEN_MSG "I have an even number of argumebts.\n"
#define ODD_MSG "I have an odd number of arguments.\n"
#define SUCCES 0

typedef int		t_bool;

#endif
